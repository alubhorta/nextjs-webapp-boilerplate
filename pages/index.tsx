import type { NextPage } from "next";
import Welcome from "../components/Welcome";

const HomePage: NextPage = () => {
  return (
    <>
      <div>HomePage</div>
      <Welcome/>
    </>

  );
};

export default HomePage;
